/*
 * FastqProcessor using MapReduce parallel programming.
 * Astrid Blaauw
 * BFV3
 */
package nl.bioinf.aiblaauw.fastqprocessor;

//import java.io.BufferedReader;
import java.io.IOException;
//import java.io.InputStream;
//import java.io.InputStreamReader;
//
//import java.nio.file.Files;
//import java.nio.file.Paths;

//import java.util.ArrayList;
//import java.util.Iterator;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;


// http://hadoop.apache.org/docs/current/hadoop-mapreduce-client/hadoop-mapreduce-client-core/MapReduceTutorial.html
// https://userinfo.surfsara.nl/systems/hadoop/usage
// http://hortonworks.com/hadoop-tutorial/using-commandline-manage-files-hdfs/

// surfsara user: "hdfs://hathi-surfsara/user/hh317787/"
// fastq file in surfsara HDFS: "/user/mherber/testset.fastq"

// run script:
// yarn jar FasqProcessor.jar input output


/**
 * Average PHRED quality score for every basepair position in FastQ reads gets calculated.
 * Using the MapReduce parallel programming method.
 *
 * @author Astrid
 */
public final class FastqProcessor {

//    private ArrayList scores;
//    private ArrayList avgList;
//    private int readLen;


    /**
     * This Mapper class maps every position of a line to it's PHRED score.
     */
    public static class TokenizerMapper
            extends Mapper<Object, Text, Text, IntWritable> {

        private final Text position = new Text();
        private int lineCount;
        private long readCount;

        @Override
        public void map(final Object key, final Text value, final Context context
        ) throws IOException, InterruptedException {

            String line = value.toString();
            LongWritable currentKey = (LongWritable) key;
            ++this.lineCount;

            if ((this.lineCount % 4) == 0) {
                this.lineCount = 0;
                ++this.readCount;

                for (int pos = 0, n = line.length(); pos < n; pos++) {
                    char c = line.charAt(pos);
                    StringBuilder sb = new StringBuilder();
                    sb.append((pos + 1));
                    position.set(sb.toString());
                    int score = (int) ((int) c / this.readCount);
                    IntWritable scoreCorr = new IntWritable(score);
                    context.write(position, scoreCorr);
                }
            }

        }
    }

    /**
     * The Reducer class combines the phred scores for every position.
     */
    public static class IntSumReducer
            extends Reducer<Text, IntWritable, Text, IntWritable> {

        private final IntWritable result = new IntWritable();

        @Override
        public final void reduce(final Text key, final Iterable<IntWritable> values,
                final Context context
        ) throws IOException, InterruptedException {
            int sum = 0;
            for (IntWritable val : values) {
                sum += val.get();
            }
            result.set(sum);
            context.write(key, result);
        }
    }

    /**
     * @param args the command line arguments
     * @throws java.io.IOException if input path is not readable
     * @throws java.lang.InterruptedException if MapReduce job interupted
     * @throws java.lang.ClassNotFoundException if MapReduce job fails
     */
    public static void main(final String[] args)
            throws IOException, InterruptedException, ClassNotFoundException {
//        FastqProcessor mainObject = new FastqProcessor();
//        mainObject.start();

        Configuration conf = new Configuration();

        Job job = Job.getInstance(conf, "process fastq");

        job.setJarByClass(FastqProcessor.class);
        job.setMapperClass(TokenizerMapper.class);
        job.setCombinerClass(IntSumReducer.class);
        job.setReducerClass(IntSumReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }


    //////

//    private ArrayList getScoresList(final String fastqPath) {
//        ArrayList outputScores = new ArrayList();
//        String read = null;
//        int lineCount = 0;
//        try (InputStream in = Files.newInputStream(Paths.get(fastqPath));
//                BufferedReader reader
//                = new BufferedReader(new InputStreamReader(in))) {
//            String line;
//            while ((line = reader.readLine()) != null) {
//                ++lineCount;
//                //System.out.println(line);
//                if ((lineCount) % 4 == 2) {
//                    read = line;
//                } else if ((lineCount) % 4 == 0) {
//                    String score = line;
//                    if (read.length() == score.length()) {
//                        ArrayList converted = this.convertScore(score);
//                        outputScores.add(converted);
//                    }
//                }
//            }
//            return outputScores;
//        } catch (IOException x) {
//            System.err.println(x);
//        }
//        return null;
//    }
//
//    private ArrayList convertScore(final String inputScore) {
//        ArrayList outputScores = new ArrayList();
//        for (int i = 0, n = inputScore.length(); i < n; i++) {
//            char c = inputScore.charAt(i);
//            outputScores.add((int) c);
//        }
//
//        return outputScores;
//    }
//
//    private ArrayList getAverage(final ArrayList scores, final int readCount, final int readLen) {
//        ArrayList averageList = new ArrayList();
//
//        for (int n = 0; n < readLen - 0; n++) {
//            int sum = 0;
//            for (Iterator itScores = scores.iterator(); itScores.hasNext();) {
//                ArrayList readScores = (ArrayList) itScores.next();
//                sum += (int) readScores.get(n);
//            }
//            averageList.add(sum / readCount);
//        }
//
//        IntStream.range(0, readLen).forEach(
//                n -> {
//                    int sum = 0;
//                    for (Iterator itScores = scores.iterator(); itScores.hasNext();) {
//                        ArrayList readScores = (ArrayList) itScores.next();
//                        sum += (int) readScores.get(n);
//                    }
//                    averageList.add(sum / readCount);
//                }
//        );
//        return averageList;
//    }
//
//    private void start() {
//        //String inPath = "/media/aiblaauw/FREECOM HDD/B_GAGTGG_L003_R1_001.fastq";
//        String inPath = "C:\\Users\\Astrid\\Dropbox\\jaar3\\T12\\BDC\\test.fastq";
//        //String inPath = "G:\\B_GAGTGG_L003_R1_001.fastq";
//        //String inPath = "/commons/docent/Thema11/Datamining_HPC/rnaseq_tss/B_GAGTGG_L003_R1_001.fastq";
//        this.scores = this.getScoresList(inPath);
//        this.readCount = this.scores.size();
//        this.readLen = ((ArrayList) this.scores.get(1)).size();
//        this.avgList = this.getAverage(this.scores, this.readCount, this.readLen);
//        System.out.println(this.avgList);
//
//    }

    //////


}
