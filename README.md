# README #

Welcome! This repo contains a Java program using the MapReduce parallel programming method. 

The project is a program written with the goal to get to know Hadoop/YARN, specifically on the SURFsara cluster. And its purose is processing a file in FastQ format, where calculations should be done with every 4th line;
every character position on those lines stands for a PHRED score and at the end the average score for every position should be calculated.

* https://userinfo.surfsara.nl/systems/hadoop
* https://github.com/sara-nl/hathi-client 
* http://wiki.apache.org/hadoop/

### How do I get set up? ###

Assuming you have a account at surfSARA (or possibly at another cluster network that supports Hadoop), installed/configured the SURFsara Hadoop environment (including Kerberos authentication) and also have the needed files available on the file system (HDFS), you have your output in a few simple steps:  

* Login

```
kinit username
```

* Run the program

```
yarn jar "/path/to/FastqProcessor.jar"
"hdfs://hathi-surfsara/user/username/inputfile.fastq" 
"hdfs://hathi-surfsara/user/username/outputfolder"
```

* Read the output

```
hadoop fs -cat "hdfs://hathi-surfsara/user/username/outputfolder/part-r-00000" 
```

### Download section? ###
The executable jar file is available!

### Dependencies? ###

These packages that provide the MapReduce behaviour are available in the *lib* folder of this project. Originally located in the folder of a installed Hadoop version.

* hadoop-mapreduce-client-core-2.7.1.jar
* hadoop-common-2.7.1.jar 

### Who do I talk to? ###

Repo owner or admin:
Astrid Blaauw [aiblaauw@gmail.com]